package model.vo;

public class Sector {

	private double longI;

	private double longF;

	private double latI;

	private double latF;
	
	private Route[] rutas;
	
	private Station[] stations;
	
	private int id;
	
	public Sector(double loi, double lof, double lai, double laf, int id) {
		longI=loi;
		longF=lof;
		latI=lai;
		latF=laf;
		rutas=new Route[0];
		stations = new Station[0];
		this.id = id;
	}

	//METODOS RETURN
	public int getID()
	{
		return id;
	}
	
	public double longInicial() {
		return longI;
	}

	public double longFinal() {
		return longF;
	}

	public double latInicial() {
		return latI;
	}

	public double latFinal() {
		return latF;
	}

	public void addRuta(Route ruta)
	{
		Route[] nueva = new Route[rutas.length+1];
		for(int i=0; i<rutas.length; i++)
		{
			nueva[i]=rutas[i];
		}
		nueva[nueva.length-1]=ruta;
		rutas = nueva;
		
	}
	
	public void addStation(Station station)
	{
			Station[] nueva = new Station[stations.length+1];
			for(int i=0; i<stations.length; i++)
			{
				nueva[i]=stations[i];
			}
			nueva[nueva.length-1]=station;
			stations = nueva;
	}
	
	public boolean hayRuta(Route ruta)
	{
		boolean gotIt = false;
		for(int i=0;i<rutas.length && !gotIt;i++)
		{
			if(rutas[i].equals(ruta))
			{
				gotIt=!gotIt;
			}
		}
		return gotIt;
	}
	
	public boolean hayStation(Station stat)
	{
		boolean gotIt = false;
		for(int i=0;i<stations.length && !gotIt;i++)
		{
			if(stations[i].equals(stat))
			{
				gotIt=!gotIt;
			}
		}
		return gotIt;
	}
	
	public Station[] getStations()
	{
		return stations;
	}
	public Route[] getRutas()
	{
		return rutas;
	}
	

}
