package model.data_structures;

import java.util.Iterator;

public class ListIterator<T> implements Iterator<T> 
{
	private Node<T> current;
	public ListIterator() {
		// TODO Auto-generated constructor stub
	}
	public boolean hasNext()  { return current != null;                     }
	public void remove()      { throw new UnsupportedOperationException();  }

	public T next() {
		if (!hasNext()) return null;
		T item = (T) current.getItem();
		current = current.getNext(); 
		return item;
	}
	public void setCurrent(Node<T> pCurrent) { current=pCurrent; }
}