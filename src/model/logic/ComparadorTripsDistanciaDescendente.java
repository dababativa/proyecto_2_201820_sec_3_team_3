package model.logic;

import java.util.Comparator;

import model.vo.Trip;

public class ComparadorTripsDistanciaDescendente implements Comparator<Trip> {

	public int compare(Trip uno, Trip dos) {
		if(uno.getDistance()>dos.getDistance())
			return -1;
		else if(uno.getDistance()<dos.getDistance())
			return 1;
		else
			return 0;

	}
}
