package model.logic;

import java.util.Comparator;

import model.vo.Trip;

public class ComparadorTripsFecha implements Comparator<Trip> {
	
	public int compare(Trip uno, Trip dos) {
		if(uno.getStartTime().isBefore(dos.getStopTime()))
			return -1;
		else if(uno.getStartTime().isAfter(dos.getStopTime()))
			return 1;
		else
			return 0;

	}
	
}
