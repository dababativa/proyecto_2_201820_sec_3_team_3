package controller;

import java.time.LocalDate;
import java.time.LocalDateTime;

import API.IManager;
import model.data_structures.LinkedList;
import model.data_structures.Queue;
import model.logic.Manager;
import model.vo.Bike;
import model.vo.ResultadoCampanna;
import model.vo.Route;
import model.vo.Station;
import model.vo.Trip;


public class Controller {
    private static IManager manager = new Manager();

    public static void cargarDatos(String dataTrips, String dataStations, String dataBikeRoutes){
    	manager.cargarDatos(dataTrips, dataStations, dataBikeRoutes);
    }
    
    public static Queue<Trip> A1(int n, LocalDate fechaTerminacion) {
        return manager.A1(n, fechaTerminacion);
    }

    public static LinkedList<Trip> A2(int n){
        return manager.A2(n);
    }

    public LinkedList<Trip> A3(int n, LocalDate localDateInicio3A) {
        return manager.A3(n, localDateInicio3A);
    }

    public LinkedList<Bike> B1(int limiteInferior, int limiteSuperior) {
        return manager.B1(limiteInferior, limiteSuperior);
    }

    public LinkedList<Trip> B2(String fechaInicial, String fechaFinal, int limiteInferiorTiempo, int limiteSuperiorTiempo) {
        return manager.B2(fechaInicial, fechaFinal, limiteInferiorTiempo, limiteSuperiorTiempo);
    }

    public int[] B3(String estacionDeInicio, String estacionDeLlegada) {
		return manager.B3(estacionDeInicio, estacionDeLlegada);
	}

    public static ResultadoCampanna C1(double valorPorPunto, int numEstacionesConPublicidad, int mesCampanna) {
		return manager.C1(valorPorPunto, numEstacionesConPublicidad, mesCampanna);
	}
    
    public double[] C2(int LA, int LO) {
		return manager.C2(LA, LO);
	}
    

	public int darSector(double latitud, double longitud) {
		return manager.darSector(latitud, longitud);
	}

	public LinkedList<Station> C3(double latitud, double longitud) {
		return manager.C3(latitud, longitud);
	}

	public LinkedList<Route> C4(double latitud, double longitud) {
		return manager.C4(latitud, longitud);
	}

	public LinkedList<Route> C5(double latitudI, double longitudI, double latitudF, double longitudF) {
		return manager.C5(latitudI, longitudI, latitudF, longitudF);
	}

	


}
